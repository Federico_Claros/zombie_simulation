//resultados.c
#include "resultados.h"

double get_porcentaje(unsigned int x, unsigned int y)
{
    return (((double) x / (double) y) * 100);
}

void print_exitos_desde_baldosa(unsigned int x, unsigned int y,
                                unsigned int cant_exitos,
                                unsigned int cant_veces)
{
    printf("\n\t[RES] Cant. exitos desde (%u,%u): %u/%u (%.2f %%).\n",
               x, y, cant_exitos, cant_veces,
               get_porcentaje(cant_exitos, cant_veces));
}

void save_zombie_position(Zombie *z, FILE *fp)
{
    if(NULL == z || NULL == fp)
      {
        printf("\n[ERR] no se pudo salvar posicion del zombie!\n");
        return;
      }
    fprintf(fp, "(%u,%u)\n", z->pos_x + 1, z->pos_y + 1);
}

unsigned int init_file(FILE **file, char *titulo)
{
    if((*file = fopen(titulo, "w")) == NULL)
      {
        printf("\n[ERR] archivo: '%s' no se pudo crear!!\n", titulo);
        return 0;
      }
    return 1;
}

void save_description(FILE *fp, const char *msg, ...)
{
    if(NULL == fp || NULL == msg)
      {
        printf("\n[ERR] No se pudo salvar descripcion!!\n");
        return;
      }
    va_list args;
    va_start(args, msg);
    vfprintf(fp, msg, args);
    va_end(args);
}
