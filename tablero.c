//tablero.c

#include "tablero.h"

void print_tablero(unsigned int t [][COLUMNAS], unsigned int filas)
{
    unsigned int fila, columna;
    for(fila = 0; fila < filas ; fila++)
      {
        for(columna = 0 ; columna < COLUMNAS ; columna++)
          {
            printf("[%d]", t[fila][columna]);
          }
        printf("\n");
      }
}

void init_tablero(unsigned int t [][COLUMNAS], unsigned int filas)
{
    unsigned int fila, columna, numero;
    numero = 0;
    for(fila = 0; fila < filas ; fila++)
      {
        numero = fila + 1;
        for(columna = 0 ; columna < COLUMNAS ; columna++)
          {
            t[fila][columna] = numero;
          }
      }
}

unsigned int is_fuera_tablero(int fila,
                              int columna)
{
    if(fila < 0 || columna < 0)
      {
        return 1;
      }
    if(columna == COLUMNAS)
      {
        return 0;
      }
    if((fila > FILAS) || (columna > COLUMNAS))
      {
        return 1;
      }

    return 0;
}
