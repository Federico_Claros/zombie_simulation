//parte_1_experimento_a.c
#include "resultados.h"
#include "progress_bar.h"

int main()
{
    unsigned int iteraccion, y, cant_veces = 0;
    unsigned int z_cant_exitos[FILAS] = {0};
    unsigned int SEED = 14;
    unsigned int tablero [FILAS][COLUMNAS];
    FILE *file = NULL;
    Zombie zombi;

    if(!init_file(&file, "coordenadas_1b.txt"))
      {
        printf("[ERR] archivo: 'coordenadas_1b.txt' no se pudo crear!!\n");
        return -1;
      }

    init_seed(SEED);
    init_tablero(tablero, FILAS);

    printf("\nParte 1, experimento B:\n");
    printf("De cual baldosa conviene partir si el objetivo es termina en la baldosa 2\n");
    printf("[MSG] Ingrese la cantidad de repeticiones: ");
    scanf("%u", &cant_veces);

    for(iteraccion = 0 ; iteraccion < cant_veces ; iteraccion++)
      {
        for(y = 0 ; y < FILAS ; y++)
          {
            init_zombie(&zombi, y, 0);
            save_zombie_position(&zombi, file);
            do
              {
                move_zombie(&zombi, tablero);
                save_zombie_position(&zombi, file);
              }
            while(!zombi.salio_laberinto);

            if(zombi.pos_x == 1)
              {
                z_cant_exitos[y]++;
              }
          }
        print_progress_bar(((double) iteraccion / (double) cant_veces));
      }
    fclose(file);

    for(y = 0; y < FILAS; y++)
      print_exitos_desde_baldosa((y+1), 1,
                                 z_cant_exitos[y], cant_veces);
    return 0;
}
