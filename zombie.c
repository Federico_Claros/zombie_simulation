//zombie.c
#include "zombie.h"

void init_seed(unsigned int seed)
{
    srand(seed);
}

void
print_pos_zombie_tablero(Zombie *z, unsigned int tablero [][COLUMNAS])
{
    unsigned int fila, columna;

    for(fila = 0; fila < FILAS; fila++)
      {
        printf("\n");

        for(columna = 0; columna < COLUMNAS; columna++)
          {
            if((z->pos_x == fila) && (z->pos_y == columna))
              {
                printf("[ ]");
              }
            else
              {
                printf("[%u]", tablero[fila][columna]);
              }
          }
      }
    printf("\n");
}

void
mover_norte(Zombie *z)
{
    z->pos_x = z->pos_x - 1;
}

void
mover_sur(Zombie *z)
{
    z->pos_x = z->pos_x + 1;
}

void
mover_este(Zombie *z)
{
    z->pos_y = z->pos_y + 1;
}

void
mover_oeste(Zombie *z)
{
    z->pos_y = z->pos_y - 1;
}

void
mover_sudeste(Zombie *z)
{
    mover_sur(z); mover_este(z);
}

void
mover_sudoeste(Zombie *z)
{
    mover_sur(z); mover_oeste(z);
}

void
mover_noreste(Zombie *z)
{
    mover_norte(z); mover_este(z);
}

void
mover_noroeste(Zombie *z)
{
    mover_norte(z); mover_oeste(z);
}

void
init_zombie(Zombie *z, unsigned int fila, unsigned int columna)
{
    if((fila >= FILAS) || (columna >= COLUMNAS))
      {
        printf("[ERR] zombie no puede iniciar en coord. (%d,%d)\n",
               fila, columna);
        return;
      }
    z->pos_x = fila;
    z->pos_y = columna;
    z->salio_laberinto = 0;
}

void
init_zombies(Zombie zombies [], unsigned int cant_zombies)
{
    for(unsigned int i = 0; i < cant_zombies; i++)
      {
        init_zombie(&zombies[i], i, 0);
      }
}

void
move_2(Zombie *z)
{
    unsigned int old_fila, old_columna;

    old_fila = z->pos_x;
    old_columna = z->pos_y;
    do
      {
        z->pos_x = old_fila;

        z->pos_y = old_columna;

        switch(rand()%3)
          {
            case 0:
                mover_sur(z);
                break;
            case 1:
                mover_sudeste(z);
                break;
            case 2:
                mover_sudoeste(z);
                break;
          }
      } while(is_fuera_tablero(z->pos_x, z->pos_y));
}

void
move_4(Zombie *z)
{
    unsigned int old_fila, old_columna;

    old_fila = z->pos_x;

    old_columna = z->pos_y;

    do
      {
        z->pos_x = old_fila;

        z->pos_y = old_columna;

        switch(rand()%3)
          {
            case 0:
                mover_norte(z);
                break;
            case 1:
                mover_noreste(z);
                break;
            case 2:
                mover_noroeste(z);
                break;
          }
      } while(is_fuera_tablero(z->pos_x, z->pos_y));
}

void
move_rnd(Zombie *z)
{
    int old_columna, old_fila;

    old_columna = z->pos_y;
    old_fila = z->pos_x;
    do
      {
         z->pos_x = old_fila;
         z->pos_y = old_columna;
        switch(rand()%8)
          {
            case NORTE:
                mover_norte(z);
                break;
            case SUR:
                mover_sur(z);
                break;
            case ESTE:
                mover_este(z);
                break;
            case OESTE:
                mover_oeste(z);
                break;
            case NOR_ESTE:
                mover_noreste(z);
                break;
            case NOR_OESTE:
                mover_noroeste(z);
                break;
            case SUD_ESTE:
                mover_sudeste(z);
                break;
            case SUD_OESTE:
                mover_sudoeste(z);
                break;
          }
      } while(is_fuera_tablero(z->pos_x, z->pos_y));
}

void
move_zombie(Zombie *z, unsigned int tablero[][COLUMNAS])
{
    if(tablero[z->pos_x][z->pos_y] == 1)
      {
        move_2(z);
        return;
      }
    if(tablero[z->pos_x][z->pos_y] == 5)
      {
        move_4(z);
        return;
      }
    move_rnd(z);

    if(z->pos_y == COLUMNAS)
      {
        z->salio_laberinto = 1;
      }
}
