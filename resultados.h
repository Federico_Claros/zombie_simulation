//resultados.h
#ifndef _RESULTADOS_H
#define _RESULTADOS_H
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "zombie.h"

double get_porcentaje(unsigned int, unsigned int);

void print_exitos_desde_baldosa(unsigned int, unsigned int,
                                unsigned int, unsigned int);

unsigned int init_file(FILE **, char *);

void save_zombie_position(Zombie *, FILE *);
void save_description(FILE *, const char *, ...);
#endif
