//demo.c
#include "resultados.h"

int main()
{
    unsigned int tablero [FILAS][COLUMNAS];
    unsigned int SEED = 14;
    char titulo [] = "coordenadas_demo.txt";

    FILE *file_pointer = NULL;

    Zombie zombi;

    if(!init_file(&file_pointer, titulo))
      {
        printf("[ERR] archivo '%s' no se pudo crear!!\n", titulo);
        return -1;
      }

    init_seed(SEED);

    init_tablero(tablero, FILAS);

    init_zombie(&zombi, 0, 0);

    save_zombie_position(&zombi, file_pointer);

    print_pos_zombie_tablero(&zombi, tablero);

    while(!zombi.salio_laberinto)
      {
        move_zombie(&zombi, tablero);

        save_zombie_position(&zombi, file_pointer);

        print_pos_zombie_tablero(&zombi, tablero);

        getchar();
      }
    save_description(file_pointer, "Zombie has escaped!!\n");

    fclose(file_pointer);

    return 0;
}
