//parte_2_invasion.c
#include <stdlib.h>
#include "resultados.h"
#include "progress_bar.h"

void recargar_one_shot(unsigned int [], unsigned int, unsigned int);

unsigned int ronda(Zombie [], unsigned int, unsigned int,
                   unsigned int, FILE *);

int main()
{
    unsigned int vez_n, cant_veces = 0;
    unsigned int cant_zombies_vivos_total = 0;
    unsigned int SEED = 14;
    FILE *file = NULL;
    Zombie zombies[FILAS];

    if(!init_file(&file, "invasion.txt"))
      {
        printf("[ERR] no se pudo crear 'invasion.txt'!!\n");
        return -1;
      }

    init_seed(SEED);
      
    printf("[MSG] Ingrese la cantidad de iteracciones: ");
    scanf("%u", &cant_veces);

    for(vez_n = 0; vez_n < cant_veces; vez_n++)
      {
        init_zombies(zombies, FILAS);
        cant_zombies_vivos_total += ronda(zombies, FILAS,
                                          FILAS, COLUMNAS, file);
        print_progress_bar((double) vez_n/ (double) cant_veces);
      }
    printf("\n[MSG] Cant. zombies vivos (en total): %u en %u iteracciones!!\n",
           cant_zombies_vivos_total, cant_veces);

    fclose(file);
    return 0;
}

unsigned int
ronda(Zombie zombies[],
      unsigned int cant_a_escapar, unsigned int cant_zombies,
      unsigned int cant_columnas, FILE *file)
{
    unsigned int cant_vivos_en_ronda = 0;
    unsigned int tablero [cant_zombies][cant_columnas];
    unsigned int v_one_shot[cant_zombies];

    recargar_one_shot(v_one_shot, cant_zombies, 1);
    init_tablero(tablero, cant_zombies);

    while(cant_a_escapar > 0)
      {
        for(unsigned int turno = 0; turno < cant_zombies ; turno++)
          {
            if(zombies[turno].salio_laberinto)
                continue;

            save_description(file, "Zombie %u has moved to: ",turno+1);

            move_zombie(&zombies[turno], tablero);

            save_zombie_position(&zombies[turno], file);

            if(zombies[turno].salio_laberinto)
              {
                cant_a_escapar--;
                if(v_one_shot[zombies[turno].pos_x] > 0)
                  {
                    v_one_shot[zombies[turno].pos_x]--;
                    save_description(file, "Zombie %u has been shot!\n",
                                     turno+1);
                    continue;
                  }
                save_description(file, "Zombie %u has escaped!\n", turno+1);

                cant_vivos_en_ronda++;
              }
          }
      }
    save_description(file, "Cant. Zombies Sobrevivieron en esta ronda: %u",
                     cant_vivos_en_ronda);

    return cant_vivos_en_ronda;
}

void
recargar_one_shot(unsigned int v_one_shot [],
                  unsigned int cant_filas,
                  unsigned int cant_recargas)
{
    for(unsigned int i = 0; i < cant_filas; i++)
        v_one_shot[i] = cant_recargas;
}
