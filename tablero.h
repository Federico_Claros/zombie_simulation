//tablero.h
#ifndef _TABLERO_H
#define _TABLERO_H

#include <stdio.h>

#define FILAS 5
#define COLUMNAS 7

void init_tablero(unsigned int [][COLUMNAS], unsigned int);
void print_tablero(unsigned int [][COLUMNAS], unsigned int);
unsigned int is_fuera_tablero(int, int);

#endif
