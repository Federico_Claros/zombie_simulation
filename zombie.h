//zombie.h
#ifndef _ZOMBIE_H
#define _ZOMBIE_H
#include <stdlib.h>
#include "tablero.h"

enum direccion
{
    NORTE,
    SUR,
    ESTE,
    OESTE,
    NOR_OESTE,
    NOR_ESTE,
    SUD_OESTE,
    SUD_ESTE
};

typedef enum direccion Direccion;

struct zombie
{
    unsigned int pos_x;
    unsigned int pos_y;
    unsigned int salio_laberinto;
};

typedef struct zombie Zombie;

void init_zombie(Zombie *, unsigned int, unsigned int);
void init_zombies(Zombie [], unsigned int);
void init_seed(unsigned int);
void mover_norte(Zombie *);
void mover_sur(Zombie *);
void mover_este(Zombie *);
void mover_oeste(Zombie *);
void mover_noroeste(Zombie *);
void mover_noreste(Zombie *);
void mover_sudeste(Zombie *);
void mover_sudoeste(Zombie *);
void print_pos_zombie_tablero(Zombie *, unsigned int [][COLUMNAS]);
void move_4(Zombie *);
void move_2(Zombie *);
void move_rnd(Zombie *);
void move_zombie(Zombie *z, unsigned int tablero[][COLUMNAS]);
#endif
