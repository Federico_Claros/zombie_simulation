//parte_1_experimento_c.c
#include "resultados.h"
#include "progress_bar.h"

int main()
{
    unsigned int I, cant_veces = 0;
    unsigned int cant_exitos = 0;
    unsigned int SEED = 14;
    unsigned int tablero [FILAS][COLUMNAS];
    FILE *file = NULL;
    Zombie zombi;

    if(!init_file(&file, "coordenadas_1c.txt"))
      {
        printf("[ERR] archivo: 'coordenadas_1c.txt' no se pudo crear!!\n");
        return -1;
      }

    init_seed(SEED);
    init_tablero(tablero, FILAS);

    printf("\nParte 1, experimento C:\n");
    printf("Saliendo de baldosa '1', que prob. hay de terminar en baldosa '5'.\n");
    printf("[MSG] Ingrese la cantidad de repeticiones: ");
    scanf("%u", &cant_veces);

    for(I = 0 ; I < cant_veces ; I++)
      {
        init_zombie(&zombi, 0, 0);
        save_zombie_position(&zombi, file);
        do
          {
            move_zombie(&zombi, tablero);
            save_zombie_position(&zombi, file);
          }
        while(!zombi.salio_laberinto);

        if(zombi.pos_x == 4)
          {
            cant_exitos++;
          }
        print_progress_bar(((double) I/ (double) cant_veces));
      }
    fclose(file);

    print_exitos_desde_baldosa(1,1, cant_exitos, cant_veces);

    return 0;
}
