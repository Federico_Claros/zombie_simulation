#!/bin/bash

# Compile progress_bar.c

x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o progress_bar.o -c -g progress_bar.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile tablero.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o tablero.o -c -g tablero.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile zombie.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o zombie.o -c -g zombie.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile resultados.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o resultados.o -c -g resultados.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile demo.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o demo.o -c -g demo.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Link demo executable
x86_64-w64-mingw32-gcc -o demo_windows_64.exe demo.o resultados.o zombie.o tablero.o
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile parte_1_experimento_a.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o p_1_e_a.o -c -g parte_1_experimento_a.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Link exp_1_a executable
x86_64-w64-mingw32-gcc -o exp_1_a_windows_64.exe p_1_e_a.o progress_bar.o resultados.o zombie.o tablero.o
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile parte_1_experimento_b.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o p_1_e_b.o -c -g parte_1_experimento_b.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Link exp_1_b executable
x86_64-w64-mingw32-gcc -o exp_1_b_windows_64.exe p_1_e_b.o resultados.o progress_bar.o zombie.o tablero.o
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile parte_1_experimento_c.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o p_1_e_c.o -c -g parte_1_experimento_c.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Link exp_1_c executable
x86_64-w64-mingw32-gcc -o exp_1_c_windows_64.exe p_1_e_c.o resultados.o progress_bar.o zombie.o tablero.o
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Compile parte_2_invasion.c
x86_64-w64-mingw32-gcc -Wall -Werror -Wextra -pedantic -o p_2.o -c -g parte_2_invasion.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Link exp_2 executable
x86_64-w64-mingw32-gcc -o exp_2_windows_64.exe p_2.o resultados.o progress_bar.o zombie.o tablero.o
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    return ${rVal} 2>/dev/null || exit "${rVal}"
fi

# Remove object files
rm -v *.o

# Success message
echo "Everything was successfully compiled for Windows!!!"
echo "Executables list:"
echo "* executable named: 'demo_windows_64.exe'"
echo "* executable named: 'exp_1_a_windows_64.exe'"
echo "* executable named: 'exp_1_b_windows_64.exe'"
echo "* executable named: 'exp_1_c_windows_64.exe'"
echo "* executable named: 'exp_2_windows_64.exe'"
