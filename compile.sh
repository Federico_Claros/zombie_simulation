#! /bin/bash

gcc -Wall -Werror -Wextra -pedantic -o tablero.o -c -g tablero.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o zombie.o -c -g zombie.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o resultados.o -c -g resultados.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o demo.o -c -g demo.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o demo demo.o resultados.o zombie.o tablero.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o progress_bar.o -c -g progress_bar.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o p_1_e_a.o -c -g parte_1_experimento_a.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o exp_1_a p_1_e_a.o resultados.o progress_bar.o zombie.o tablero.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o p_1_e_b.o -c -g parte_1_experimento_b.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o exp_1_b p_1_e_b.o resultados.o progress_bar.o zombie.o tablero.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o p_1_e_c.o -c -g parte_1_experimento_c.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o exp_1_c p_1_e_c.o resultados.o progress_bar.o zombie.o tablero.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -Wall -Werror -Wextra -pedantic -o p_2.o -c -g parte_2_invasion.c

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

gcc -o exp_2 p_2.o resultados.o progress_bar.o zombie.o tablero.o

rVal=$?

if [ "${rVal}" -ne 0 ] ; then
    return ${rVal} 2>/dev/null
    exit "${rVal}"
fi

rm -v *.o

echo "Everything was successfully compiled!!!"
echo "Executables list:"
echo "* executable named: 'demo'"
echo "* executable named: 'exp_1_a'"
echo "* executable named: 'exp_1_b'"
echo "* executable named: 'exp_1_c'"
echo "* executable named: 'exp_2'"
