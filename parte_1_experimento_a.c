//parte_1_experimento_a.c
#include "progress_bar.h"
#include "resultados.h"

int main()
{
    unsigned int vez_n, cant_veces = 0;
    unsigned int cant_exitos = 0;
    unsigned int SEED = 14;
    unsigned int tablero [FILAS][COLUMNAS];
    FILE *file = NULL;
    Zombie zombi;

    init_tablero(tablero, FILAS);
    
    if(!init_file(&file, "coordenadas_1a.txt"))
      {
        printf("[ERR] archivo: 'coordenadas_1a.txt' no se pudo crear!!\n");
        return -1;
      }

    init_seed(14);

    printf("\nParte 1, experimento A:\n");
    printf("Saliendo de baldosa '3', que prob. hay de terminar en baldosa '3'.\n");
    printf("[MSG] Ingrese la cantidad de repeticiones: ");
    scanf("%u", &cant_veces);

    for(vez_n = 0 ; vez_n < cant_veces ; vez_n++)
      {
        init_zombie(&zombi, 2, 0);
        save_zombie_position(&zombi, file);
        do
          {
            move_zombie(&zombi, tablero);
            save_zombie_position(&zombi, file);
          }
        while(!zombi.salio_laberinto);

        if(zombi.pos_x == 2)
          {
            cant_exitos++;
          }
        print_progress_bar(((double) vez_n / (double) cant_veces));
      }
    fclose(file);

    print_exitos_desde_baldosa(3,1, cant_exitos, cant_veces);

    return 0;
}
